'''ЗАДАНИЕ 1: И снова гласные.
УСЛОВИЕ: Посчитать количество гласных в каждом слове текста.
Вывести максимальное количество гласных в одном слове.
ТЕКСТ:
Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta.
Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis
quis ac lectus. Donec rutrum congue leo eget malesuada.
ГЛАСНЫЕ: A, E, I, O, U, Y'''

# import re
#
# sentence = 'Proin eget tortor risus. Cras ultricies ligula sed magna dictum ' \
#            'porta. Proin eget tortor risus. Curabitur non nulla sit amet ' \
#            'nisl tempus convallis № # quis ac lectus. Donec rutrum congue ' \
#            'leo eget mAlesuada.'
#
# # out = re.sub(r'[^\w\s]', '', sentence)
#
# wordlist = sentence.replace('.', '').split()
#
# for word in wordlist:
#     # w = list(word)
#     i = 0
#     arr = []
#     # for l in w:
#     for l in word:
#         # if l in ['a', 'e', 'i', 'o', 'u', 'y']:
#         if l.lower() in 'aeiouy':
#             i += 1
#     arr.append(i)
#     print(word, " - ", i)
# print('Максимальное количество гласных в одном слове: ', max(arr))

#РЕШЕНИЕ С ИСПОЛЬЗОВАНИЕМ ФУНКЦИИ

# import re
#
# def vowels(sentence):
#     out = re.sub(r'[^\w\s]', '', sentence)
#     wordlist = out.split()
#     for word in wordlist:
#         w = list(word)
#         i = 0
#         arr = []
#         for l in w:
#             if l in ['a', 'e', 'i', 'o', 'u', 'y']:
#                 i += 1
#         arr.append(i)
#         print(word, " - ", i)
#     print('Максимальное количество гласных в одном слове: ', max(arr))
#
# sentence = input('Enter the sentence: ')
# vowels(sentence)

'''ЗАДАНИЕ 2: Самое длинное слово.
УСЛОВИЕ: Найти слово максимальной длины в тексте. Вывести это слово. Если таких слов несколько - вывести все.
ТЕКСТ: Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada.'''

# import re
#
# # sentence = 'Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis № # quis ac lectus. Donec rutrum congue leo eget malesuada.'
#
# # sentence = 'Мама мыла раму и запивала'
# sentence = 'Mama ama criminal, marzepan'
# out = re.sub(r'[^\w\s]', '', sentence)
#
# wordlist = out.split()
#
# max_qty = len(max(wordlist))
# print('Максимальное слово(а) в предложении: ')
# for word in wordlist:
#     if len(word) == max_qty:
#         print(word)

'''ЗАДАНИЕ 3: Реверс'em!
УСЛОВИЕ: Изменить в тексте порядок следования: - букв в словах; - слов в предложениях; - предложений в тексте. Вывести модифицированный текст.
ТЕКСТ: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta. '''

import re

# my_text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta. '
# my_text = 'Мама мыла раму и запивала. Папа мыл машину'
my_text = 'mama mila ramu. papa pil'
out = re.sub(r'[^\w\s\d\.]', '', my_text)

s_list = out.split('. ')
s_list.reverse()
s_list.pop(0)
rs_list = []
for s in s_list:
    w_list = s.split()
    w_list.reverse()
    rw_list = []
    for l in w_list:
        l_list = list(l)
        l_list.reverse()
        low = [x.lower() for x in l_list]
        rw = ''.join(low)
        rw_list.append(rw)
    rw_list[0] = rw_list[0].title()
    rs = ' '.join(rw_list)
    rs_list.append(rs)

print('. '.join(rs_list),'.')














