
def simple_reverse(text):
    sentenses = text.split(".")
    sentenses.reverse()
    worded_sentenses = []
    for sentense in sentenses:
        words = sentense.split()
        words.reverse()
        worded_sentenses.append(words)
    result = []
    for sentense in worded_sentenses:
        for word in sentense:
            result.append(word[::-1])
    result = " ".join(result)
    return result

result = simple_reverse('Мама мыла раму. А папа мыл окно')
print(result)
