#УСЛОВИЕ:
#Найти слово максимальной длины в тексте.
#Вывести это слово. Если таких слов несколько - вывести все.
#Текст:
#Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Curabitur non
#nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada.
def max_lenght (text):
    print ('Task 2\n',text,'\n')
    temp = text.replace('.','')        
    temp = temp.split()    
    max_len = 0
    for i in temp:
        if len(i) > max_len:
            max_len = len(i)
    for i in temp:
        if len(i) == max_len:
            print(i)   