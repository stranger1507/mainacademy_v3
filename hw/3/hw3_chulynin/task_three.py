#УСЛОВИЕ:
#Изменить в тексте порядок следования:
#- букв в словах;
#- слов в предложениях;
#- предложений в тексте.
#Вывести модифицированный текст.
#Текст:
#Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Lorem
#ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Cras ultricies
#ligula sed magna dictum porta.
def reverse (text):
    print ('Task 3\n',text,'\n')
    temp = text.split('.')
    temp.reverse()
    #temp.remove(temp[0])
    #print(temp)
    rev_words = list()
    for i in temp:
        word = i.split()
        word.reverse()
        rev_words.append(word)
    #print(rev_words)
    rev_all = list()
    for i in rev_words:
        for j in i:
            rev_all.append(j[::-1])
    print(" ".join(rev_all))
