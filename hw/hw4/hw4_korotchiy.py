from datetime import datetime, date


class Person:
    surname = 'Коротчий'
    first_name = 'Максим'
    nickname = 'maximkorotchiy'
    birth_date = datetime.strptime('2001-04-05', "%Y-%m-%d")
    aa = None
    
    def get_age(self):
        born = self.birth_date
        today = date.today()
        print('Возраст контакта в полных годах на дату вызова:')
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    
    def get_fullname(self):
        surname = self.surname
        first_name = self.first_name
        nickname = self.nickname
        print('Полное имя контакта: ')
        # return surname + ' ' + first_name + ' ' + '(' + nickname + ')'
        return ' '.join([surname, first_name, nickname])


a = Person()
print(a.get_age())
print(a.get_fullname())

# Огромное спасибо за поддержание формата дз
