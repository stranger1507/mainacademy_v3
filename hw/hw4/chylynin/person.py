from datetime import date, datetime


class Person(object):
    """description of class"""
    
    def __init__(self, surname, first_name, birth_date, nickname=None):
        self.__surname = surname
        self.__first_name = first_name
        try:
            try_birth_date = datetime.strptime(birth_date, '%d.%m.%Y')
            self.__birth_date = try_birth_date.date()
        except:
            self.__birth_date = date(1980, 1, 1)
            print(
                'Entered date is not correct. Birthday is set for 01.01.1980')
        if nickname != None:
            self.__nickname = nickname
    
    @property
    def surname(self):
        return self.__surname
    
    @surname.setter
    def surname(self, surname):
        self.__surname = surname
    
    @property
    def first_name(self):
        return self.__first_name
    
    @first_name.setter
    def first_name(self, first_name):
        self.__first_name = first_name
    
    @property
    def birth_date(self):
        return self.__birth_date
    
    @birth_date.setter
    def birth_date(self, birth_date):
        try:
            try_birth_date = datetime.strptime(birth_date, '%d.%m.%Y')
            self.__birth_date = try_birth_date.date()
        except ValueError:
            raise ValueError('Entered date is not correct.')
    
    @property
    def nickname(self):
        return self.__nickname
    
    @nickname.setter
    def nickname(self, nickname):
        self.__nickname = nickname
    
    def get_age(self):
        today = date.today()
        return today.year - self.__birth_date.year
    
    def get_fullname(self):
        return '{} {}'.format(self.__first_name, self.__surname)
    
