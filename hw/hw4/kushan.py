from datetime import date


class Person:
    surname = ""
    first_name = ""
    nickname = ""
    birth_date = date(1, 1, 1)
    
    def __init__(self, surname, first_name, nickname, birth_data):
        self.surname = surname
        self.first_name = first_name
        self.nickname = nickname
        self.birth_date = birth_data
        print(surname, first_name, nickname, birth_data)
    
    def get_fullname(self):
        return self.surname + ' ' + self.first_name
    
    def get_age(self):
        today = date.today()
        return today.year - self.birth_date.year


a = Person(str('Samarasinghe'), str('Kushan'), 'red', date(1994, 4, 17))

print(a.get_fullname())
print(a.get_age())
# ============================================================================
# from _datetime import date, datetime
#
# class Person:
#     def __init__(self, surname, first_name, nickname, birth_data):
#         self.surname = surname
#         self.first_name = first_name
#         self.nickname = nickname
#         self.birth_date = datetime.strptime(birth_data, '%y,%m,%d').date()
#         print(surname, first_name, nickname, self.birth_date)
#
#     def get_fullname(self):
#         return self.surname + ' ' + self.first_name
#
#     def get_age(self):
#         today = date.today()
#         return today.year - self.birth_date.year
#
# a = Person(str('Samarasinghe'), str('Kushan'), 'red', str('94,4,17'))
#
# print(a.get_fullname())
# print(a.get_age())
