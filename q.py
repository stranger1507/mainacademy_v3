from datetime import datetime, date

class Person:

    surname = 'korotchiy'
    first_name = 'max'
    birth_date = '2001-04-05'
    nickname = None

    def __init__(self, surname=surname, first_name=first_name, birth_date=birth_date,
                 nickname=None):
        self.surname = surname
        self.first_name = first_name
        self.nickname = nickname
        self.birth_date = birth_date

    def get_fullname(self):
        return Person.surname + ' ' + Person.first_name + ' ' + str(Person.nickname)

    def get_age(self):
        today = date.today()
        try:
            birth_date = datetime.strptime(Person.birth_date, "%Y-%m-%d")
        except Exception:
            print("ee")
        return today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))

a = Person()
print(a.get_fullname())
print(a.get_age())

