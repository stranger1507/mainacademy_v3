"""Random"""
import random
# print(random.randint(1, 10))
# print(random.random())
# print(random.choice([2,4,5,66,7]))
# a = [2,4,5,66,7]
# random.shuffle(a)
# print(a)
"""Math"""
import math
# print(math.fsum([1,2.5, 2.3453]))
# print(math.floor(1.6))
# print(math.ceil(1.6))
# print(math.fabs(-33))
# print(math.sqrt(4))
# print(math.pow(4, 3))
# print(math.cos(1))
# print(math.pi)
# print(math.e)
"""OS"""
import os
# print(os.name)
# print(os.getcwd())

""" os.path """

print(os.path.abspath('..'))
print(os.path.dirname('~'))
print(os.path.isfile('/usr') )
print(os.path.isdir('/usr') )

print(os.path.join('~', 'vlad','tet'))
print(os.path.split(os.path.abspath('..')))