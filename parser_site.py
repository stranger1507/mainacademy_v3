import requests
from bs4 import BeautifulSoup

url = 'https://ithappens.me/'

response = requests.get(url)

data = []

if response.status_code == 200:
    soup = BeautifulSoup(response.text, 'html.parser')
    for div in soup.findAll('div', class_='story'):
        records = {}
        for title in div.findAll('h2'):
            records['title'] = title.text
        for p in div.findAll('p'):
            records['text'] =  p.text

        with open('ithapens.txt', 'a') as fp:
            fp.write(records['title'].encode('utf-8'))
            fp.write('\n')
            fp.write(records['text'].encode('utf-8'))
            fp.write('\n')
            fp.write('='*50)
            fp.write('\n')