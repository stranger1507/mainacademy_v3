FROM python:3.7

WORKDIR /web

COPY mysite/requirements.txt /requirements.txt
COPY mysite/requirements_dev.txt /requirements_dev.txt

RUN pip install -r /requirements_dev.txt

RUN rm /requirements_dev.txt /requirements.txt