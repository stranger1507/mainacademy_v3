from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UsernameField


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ("email", "is_staff", "is_superuser")
        field_classes = {'email': UsernameField}
