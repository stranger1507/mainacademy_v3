from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        from mysite.celery_app import debug_task
        result = debug_task.delay()
        print(result)