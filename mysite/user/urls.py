from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from user.views import UserUpdateView, UserCreateView

urlpatterns = [
    path('update/', UserUpdateView.as_view(), name='user_edit'),
    path('registration/', UserCreateView.as_view(), name='user_registration'),
    path('login/', LoginView.as_view(template_name='user/update.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
]
