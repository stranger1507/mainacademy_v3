from braces.views import AnonymousRequiredMixin

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView, CreateView

from user.forms import UserRegistrationForm

User = get_user_model()


class UserUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'user/update.html'
    success_url = '/'
    fields = '__all__'

    def get_object(self, queryset=None):
        return self.request.user


class UserCreateView(AnonymousRequiredMixin, CreateView):
    queryset = User.objects.all()
    template_name = 'user/update.html'
    success_url = '/'
    form_class = UserRegistrationForm
