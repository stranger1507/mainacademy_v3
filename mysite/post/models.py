from django.conf import settings
from django.db import models
from django.db.models import signals
from django.utils.translation import ugettext_lazy as _


class Post(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(_('Заголовок'), max_length=255)
    preview = models.TextField(_('Анонс новости'))
    image = models.ImageField(
        _('Изображение'), upload_to='post/images/', null=True, blank=True
    )
    text = models.TextField(_('Текст новости'))
    created_datetime = models.DateTimeField(
        _('Дата создания'), auto_now_add=True
    )
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Автор'),
                               on_delete=models.CASCADE)
    updated_datetime = models.DateTimeField(_('Дата обновления'), auto_now=True)
    is_published = models.BooleanField(_('Статус публикации'), default=False)

    def delete(self, using=None, keep_parents=False, force_delete=False):
        if not force_delete:
            self.is_published = False
            self.save(update_fields=['is_published'])
        return super(Post, self).delete(using=None, keep_parents=False)

    def save(self, **kwargs):
        if not self.pk:
            self.is_published = True
        return super(Post, self).save(**kwargs)

    class Meta:
        verbose_name = _('Новость')
        verbose_name_plural = _('Новости')
        ordering = ('-created_datetime',)

    def __str__(self):
        return 'Новость:{0}'.format(self.title)


class Comment(models.Model):
    comment = models.TextField(_('Текст комментария'))
    created_datetime = models.DateTimeField(
        _('Дата создания'), auto_now_add=True
    )
    updated_datetime = models.DateTimeField(
        _('Дата обновления'), auto_now=True
    )
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Автор'),
                               on_delete=models.CASCADE)
    post = models.ForeignKey(
        Post, verbose_name=_('Новость'), on_delete=models.CASCADE,
        related_name='comments'
    )
    parent = models.ForeignKey(
        'self', null=True, blank=True, on_delete=models.SET_NULL,
        related_name='answer'
    )


    class Meta:
        verbose_name = _('Комментарий')
        verbose_name_plural = _('Комментарии')
        ordering = ('-created_datetime',)

    def __str__(self):
        return 'Комментарий к {0}'.format(self.post.title)

