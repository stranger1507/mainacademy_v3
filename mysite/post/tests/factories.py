import factory
from factory.fuzzy import FuzzyText

from post.models import Post
from user.tests.factories import UserFactory


class PostFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Post
    title = FuzzyText(length=250)
    preview = FuzzyText(length=250)
    image = factory.django.ImageField(color='red')
    text = FuzzyText()
    author = factory.SubFactory(UserFactory)
