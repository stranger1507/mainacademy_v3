from unittest.mock import patch

from django.test import TestCase
from rest_framework.exceptions import ValidationError

from post.serializers import PostSerializer
from post.tests.factories import PostFactory


class TestPostSerializer(TestCase):
    def setUp(self):
        self.post = PostFactory()

    def test_created_datetime(self):
        serializer = PostSerializer(instance=self.post, context=self.client.request().context)
        self.assertEqual(
            self.post.created_datetime.date(),
            serializer.data['created_datetime']
        )

    def test_post_image_with_image(self):
        context = self.client.request().context
        serializer = PostSerializer(instance=self.post, context=context)
        self.assertEqual(
            serializer.data['post_image'],
            context['request'].build_absolute_uri(self.post.image.url)
        )

    def test_post_image_without_image(self):
        context = self.client.request().context
        self.post.image = None
        self.post.save()
        serializer = PostSerializer(instance=self.post, context=context)
        self.assertEqual(
            serializer.data['post_image'],
            'https://opt-316320.ssl.1c-bitrix-cdn.ru/upload/iblock'
            '/a41/a41458b501efbf0fd55e44a34a7bcd9a.jpg?1516776495120316'
        )

    def test_validate_preview_with_raise_error(self):
        context = self.client.request().context
        serializer = PostSerializer(instance=self.post, context=context)
        with self.assertRaises(ValidationError) as context:
            serializer.validate_preview('привет')
            self.assertIn('Плохой текст' in context.exception)

    def test_validate_preview(self):
        context = self.client.request().context
        serializer = PostSerializer(instance=self.post, context=context)
        self.assertEqual(
            serializer.validate_preview('test'),
            'test'
        )

    def test_get_today(self):
        with patch('post.serializers.timezone', return_value='timezone') as timezone:
            result = 'timezone.now'
            timezone.now.side_effect = lambda *x: result
            context = self.client.request().context
            serializer = PostSerializer(instance=self.post, context=context)
            self.assertEqual(serializer.data['today'], result)

        # serializer = PostSerializer(instance=self.post, context=context)
        # print(serializer.data['today'])

    @patch('post.serializers.timezone', return_value='timezone')
    def test_today_v2(self, timezone):
        result = 'timezone.now'
        timezone.now.side_effect = lambda *x: result
        context = self.client.request().context
        serializer = PostSerializer(instance=self.post, context=context)
        self.assertEqual(serializer.data['today'], result)
