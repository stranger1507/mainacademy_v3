from unittest.mock import patch

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from post.models import Post
from post.serializers import PostSerializer
from post.tests.factories import PostFactory
from user.tests.factories import UserFactory


class TestPostViewSet(APITestCase):
    def setUp(self) -> None:
        PostFactory.create_batch(5)
        self.user = UserFactory()
        self.client.force_login(self.user)

    @patch('post.serializers.timezone', return_value='timezone')
    def test_list(self, mock):
        url = reverse('post-list')
        result = 'timezone.now'
        mock.now.side_effect = lambda *x: result
        response = self.client.get(url)
        posts = Post.objects.all()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertSequenceEqual(
            response.data['results'],
            PostSerializer(
                posts, many=True, context=self.client.request().context
            ).data
        )

    @patch('post.serializers.timezone', return_value='timezone')
    def test_create(self, mock):
        url = reverse('post-list')

        result = 'timezone.now'
        mock.now.side_effect = lambda *x: result
        data = {'title': 'title', 'preview': 'preview', 'text': 'text', 'author': self.user.id}
        response = self.client.post(url, data)
        post = Post.objects.order_by('id').last()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(
            response.data,
            PostSerializer(
                post, context=self.client.request().context
            ).data
        )

    @patch('post.serializers.timezone', return_value='timezone')
    def test_patch(self, mock):
        post = PostFactory()
        url = reverse('post-detail', args=[post.id])

        result = 'timezone.now'
        mock.now.side_effect = lambda *x: result
        data = {'title': 't'}
        response = self.client.patch(url, data)

        post.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            response.data,
            PostSerializer(
                post, context=self.client.request().context
            ).data
        )

    def test_destroy(self):
        post = PostFactory()
        url = reverse('post-detail', args=[post.id])

        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Post.objects.filter(id=post.id).exists())
