from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer

from post.models import Post, Comment


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = ('comment', 'post', 'parent')


class PostSerializer(ModelSerializer):
    all_comments = CommentSerializer(
        source='comments', many=True, read_only=True
    )
    post_image = serializers.SerializerMethodField()
    created_datetime = serializers.SerializerMethodField()
    today = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = '__all__'

    def get_created_datetime(self, obj):
        return obj.created_datetime.date()

    def get_today(self, obj):
        return timezone.now()

    def get_post_image(self, obj):
        if obj.image:
            return self.context['request'].build_absolute_uri(obj.image.url)
        return 'https://opt-316320.ssl.1c-bitrix-cdn.ru/upload/iblock' \
               '/a41/a41458b501efbf0fd55e44a34a7bcd9a.jpg?1516776495120316'

    def validate_preview(self, preview):
        if 'привет' in preview.lower():
            raise ValidationError('Плохой текст')
        return preview