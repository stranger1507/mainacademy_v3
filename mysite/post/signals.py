from django.db.models import signals

from post.models import Post


def deleted_posts(sender, instance=None, **kwargs):
    instance.is_published = False
    instance.save(update_fields=['is_published'])

signals.pre_delete(deleted_posts, Post)