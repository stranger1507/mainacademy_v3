from django import forms


class SumForm(forms.Form):
    a = forms.IntegerField()
    b = forms.IntegerField()


class DivForm(forms.Form):
    error_css_class = 'error'
    required_css_class = 'required '

    c = forms.IntegerField(required=False)
    d = forms.IntegerField(
        label='D label',
        widget=forms.NumberInput(
            attrs={
                'class': 'text', 'placeholder': 'Enter D',
                'style': 'width:10px'
            }
        )
    )

    def clean_d(self):
        d = self.cleaned_data.get('d')
        if str(d).isdigit() and d == 0:
            raise forms.ValidationError('D должно быть больше или меньше 0')
        return d

    def clean(self):
        if self.cleaned_data.get('c') and self.cleaned_data.get('d') and \
                self.cleaned_data['c'] < self.cleaned_data['d']:
            self.add_error('c', 'C должно быть больше D')
        return super(DivForm, self).clean()

    def is_valid(self):
        data = super(DivForm, self).is_valid()
        for field in self.errors:
            self.fields[field].widget.attrs.update({'color': 'red'})
        return data