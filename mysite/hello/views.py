from django.http import HttpResponse
from django.shortcuts import render

from hello.forms import SumForm, DivForm


def first_run(request):
    if request.POST:
        context = {}
        form = SumForm(request.POST, prefix='form')
        form2 = DivForm(request.POST, prefix='form2')
        if not form.is_valid():
            context.update({'form': form})
        else:
            a = form.cleaned_data['a']
            b = form.cleaned_data['b']
            try:
                sum = int(a) + int(b)
            except:
                sum = ''
            context.update({'form': form, 'a': a, 'b': b, 'sum': sum, })
        if not form2.is_valid():
            context.update({'form2': form2})
        else:
            d = form2.cleaned_data['d']
            c = form2.cleaned_data['c']
            try:
                divide = c / d
            except:
                divide = ''
            context.update({'form2': form2, 'c': c, 'd': d, 'div': divide})
        return render(request, 'index.html', context)
    return render(request, 'index.html', {'form': SumForm(prefix='form'), 'form2': DivForm(prefix='form2')})
