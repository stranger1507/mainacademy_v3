from django.http import HttpResponse
from django.urls import path

from hello.views import first_run

urlpatterns = [
    path('', first_run),
]
