from __future__ import absolute_import, unicode_literals

import os
from datetime import timedelta


from celery import Celery, shared_task

# set the default Django settings module for the 'celery' program.
from celery.task import periodic_task
from django.utils import timezone

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings.base')

app = Celery()


# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings', namespace='CELERY')

# There is no need to pass args to autodiscover func in Celery >4.x
app.autodiscover_tasks()


@shared_task
def debug_task():
    print('Request: TTT')


@periodic_task(run_every=timedelta(minutes=10))
def test_beat():
    return timezone.now()