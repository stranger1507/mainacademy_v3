from .base import *

AUTH_PASSWORD_VALIDATORS = []

INSTALLED_APPS += (
    'django_extensions',
    'debug_toolbar',
)

MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'postgres',
#         'USER': 'postgres',
#         'PASSWORD': 'test37',
#         'HOST': 'db',
#         'PORT': '5432',
#         'ATOMIC_REQUESTS': True
#     }
# }
AUTH_USER_MODEL = 'user.User'