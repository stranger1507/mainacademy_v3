-r requirements.txt

django-extensions==2.1.9
django-debug-toolbar==2.0
coverage
ipdb
factory-boy==2.12.0